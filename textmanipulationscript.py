import re
import os
import shutil

logfile=open("hosts.real", "r")
report=open("hosts.new", "w")
ip=open("ip.txt", "w")
commentRegex=re.compile("#")

for line in logfile:
    # ONE removes all comments
    if commentRegex.search(line):
        line=line.rstrip("\n\r")
        line=re.sub("#.*","",line)

    # TWO extract MAC addresses
    data=line.split()
    try:
        line=line.rstrip('\n\r')
        mac=data.pop()
        print(mac)
    except:
        pass

    # THREE remove all the extraneous spaces
    newline=re.sub('\s+',' ', line).strip()
    if len(newline.strip()) != 0:
        report.write(newline+"\n")

    # FOUR write the IP addresses to a new file
    try:
        tmp=line.split()
        ipAdd=tmp.pop(0) + "\n"
        ip.write(ipAdd)
    except:
        pass
logfile.close()
report.close()
ip.close()
os.remove('hosts.real')
os.rename('hosts.new', 'hosts.real')
